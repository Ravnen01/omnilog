package com.lecor.omnilog

import android.app.Application
import com.lecor.omnilog.data.manager.ApiManager
import com.lecor.omnilog.data.repository.ListArticleRepository
import com.lecor.omnilog.presentation.feature.listeArticle.ListArticleViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class OmnilogApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        initializeInjection()
    }

    private fun initializeInjection(){
        val apiManager = ApiManager()
        val myViewModels = module{
            factory{ListArticleViewModel(ListArticleRepository(apiManager))}
        }

        startKoin {
            androidContext(this@OmnilogApplication)
            modules(myViewModels)
        }
    }
}