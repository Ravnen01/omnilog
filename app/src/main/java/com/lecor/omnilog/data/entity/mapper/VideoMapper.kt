package com.lecor.omnilog.data.entity.mapper

import com.lecor.omnilog.data.entity.VideoEntity
import com.lecor.omnilog.presentation.model.Video
import java.util.*

fun VideoEntity.transform():Video{
    return Video(
        title,
        thumb,
        url,
        Date(date.toLong()*1000),
        sport.name,
        views
    )
}

fun List<VideoEntity>.transform(): List<Video>{
    val list = mutableListOf<Video>()
    this.forEach{
        list.add(it.transform())
    }
    return list
}