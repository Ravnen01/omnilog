package com.lecor.omnilog.data.entity

import com.google.gson.annotations.SerializedName

data class ListArticleEntity(
    @SerializedName("videos")
    val listVideoEntity: List<VideoEntity>,
    @SerializedName("stories")
    val listStoriesEntity: List<StoriesEntity>
) {
}