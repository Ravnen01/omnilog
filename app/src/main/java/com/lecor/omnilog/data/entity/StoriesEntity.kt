package com.lecor.omnilog.data.entity

data class StoriesEntity(
    val id:Int,
    val title:String,
    val teaser:String,
    val image:String,
    val date:Double,
    val author:String,
    val sport:SportEntity
) {
}