package com.lecor.omnilog.data.repository

import com.lecor.omnilog.data.entity.ListArticleEntity
import com.lecor.omnilog.data.entity.mapper.transform
import com.lecor.omnilog.data.manager.ApiManager
import com.lecor.omnilog.presentation.`interface`.Article

class ListArticleRepository(private val baseManager: ApiManager) {

    fun getListArticle(): List<Article> {
        val list = mutableListOf<Article>()
        return try {
            val listEntity: ListArticleEntity = baseManager.getListArticle()
            //TODO: Le tri ne doit pas etre ici, revoir le passage d'article
            list.addAll(listEntity.listStoriesEntity.sortedByDescending { it.date }.transform())
            var index = 1
            for (it in listEntity.listVideoEntity.sortedByDescending { it.date }.transform()) {
                list.add(index, it)
                index += 2
            }
            list
        } catch (e: Throwable) {
            list
        }

    }
}