package com.lecor.omnilog.data.entity.mapper

import com.lecor.omnilog.data.entity.StoriesEntity
import com.lecor.omnilog.presentation.model.Stories
import java.sql.Date


fun StoriesEntity.transform():Stories{
    return Stories(
        title,
        teaser,
        image,
        Date(date.toLong()*1000),
        author,
        sport.name
    )
}

fun List<StoriesEntity>.transform(): List<Stories>{
    val list = mutableListOf<Stories>()
    this.forEach{
        list.add(it.transform())
    }
    return list
}
