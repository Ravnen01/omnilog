package com.lecor.omnilog.data.manager

import com.google.gson.Gson
import com.lecor.omnilog.data.entity.ListArticleEntity
import com.lecor.omnilog.data.entity.StoriesEntity
import com.lecor.omnilog.data.entity.VideoEntity
import com.lecor.omnilog.data.utils.URL
import com.lecor.omnilog.presentation.`interface`.Article
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

class ApiManager()  {
    private val gson = Gson()
    var client: OkHttpClient = OkHttpClient.Builder()
        .build()

    fun getListArticle(): ListArticleEntity {
        val request: Request = Request.Builder()
            .url("$URL/edfefba")
            .build()
        return client.newCall(request).execute().use { response ->
            if (!response.isSuccessful) throw IOException("Unexpected code $response")
            return try {
                val response=response.body?.string()
                gson.fromJson(response, ListArticleEntity::class.java)
            }catch (e: Throwable){
                e.printStackTrace()
                ListArticleEntity(listOf<VideoEntity>(),listOf<StoriesEntity>())
            }
        }
    }
}