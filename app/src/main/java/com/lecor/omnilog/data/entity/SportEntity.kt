package com.lecor.omnilog.data.entity

data class SportEntity(
    val id:Int,
    val name:String
) {
}