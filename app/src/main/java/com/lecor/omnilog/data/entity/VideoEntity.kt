package com.lecor.omnilog.data.entity
 data class VideoEntity(
     val id:Int,
     val title:String,
     val thumb:String,
     val url:String,
     val date:Double,
     val sport:SportEntity,
     val views:Int
 ){
}