package com.lecor.omnilog.presentation.feature.listeArticle

import android.content.Context
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lecor.omnilog.R
import com.lecor.omnilog.presentation.`interface`.Article
import com.lecor.omnilog.presentation.model.Video
import kotlinx.android.synthetic.main.item_list_article.view.*

class ListArticleRecyclerAdapter(private val context: Context, private val onClickListener: OnItemClickListener) :
    RecyclerView.Adapter<ListArticleRecyclerAdapter.ViewHolder>() {
    private var listArticle = listOf<Article>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.picture_article
        val title: TextView = itemView.text_title
        val sport: TextView = itemView.text_sport
        val lastLine: TextView = itemView.text_last_line
        val picturePlay: ImageView = itemView.picture_play
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_list_article, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = listArticle[position].getTitleArticle()
        holder.sport.text = listArticle[position].getSportArticle()
        holder.lastLine.text = listArticle[position].getLastLine()

        Glide
            .with(holder.itemView)
            .load(listArticle[position].getImageArticle())
            .placeholder(android.R.drawable.ic_menu_report_image)
            .into(holder.image)

        if(listArticle[position] is Video){
            holder.picturePlay.visibility = View.VISIBLE
        }else{
            holder.picturePlay.visibility = View.INVISIBLE
        }
        holder.itemView.setOnClickListener {
            onClickListener.onItemClicked(listArticle[position])
        }
    }

    override fun getItemCount(): Int {
        return listArticle.size
    }

    fun setListArticle(templistArticle: List<Article>){
        listArticle=templistArticle
    }

    interface OnItemClickListener {
        fun onItemClicked(
            article: Article
        )
    }

}