package com.lecor.omnilog.presentation.model

import android.os.Parcelable
import com.lecor.omnilog.data.entity.SportEntity
import com.lecor.omnilog.presentation.`interface`.Article
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Video(
    val title:String,
    val thumb:String,
    val url:String,
    val date:Date,
    val sport: String,
    val views:Int
) : Article , Parcelable{
    override fun getTitleArticle(): String {
        return title
    }

    override fun getImageArticle(): String {
        return thumb
    }

    override fun getSportArticle(): String {
        return sport
    }

    override fun getLastLine(): String {
       return "$views views"
    }
}