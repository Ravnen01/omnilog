package com.lecor.omnilog.presentation.model

import android.os.Parcelable
import android.text.format.DateUtils
import com.lecor.omnilog.data.entity.SportEntity
import com.lecor.omnilog.presentation.`interface`.Article
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Stories (
    val title:String,
    val teaser:String,
    val image:String,
    val date: Date,
    val author:String,
    val sport: String
) :Article , Parcelable{
    override fun getTitleArticle(): String {
        return title
    }

    override fun getImageArticle(): String {
        return image
    }

    override fun getSportArticle(): String {
        return sport
    }

    override fun getLastLine(): String {
        val timeAgo= DateUtils.getRelativeTimeSpanString(date.time,Calendar.getInstance().timeInMillis,DateUtils.MINUTE_IN_MILLIS)
        return "By $author - $timeAgo"
    }
}