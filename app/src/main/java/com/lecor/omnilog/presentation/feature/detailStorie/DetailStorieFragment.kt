package com.lecor.omnilog.presentation.feature.detailStorie

import android.os.Bundle
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.lecor.omnilog.R
import kotlinx.android.synthetic.main.detail_storie_fragment.*
import java.util.*

class DetailStorieFragment : Fragment() {

    private val args: DetailStorieFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detail_storie_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide
            .with(view)
            .load(args.storie.image)
            .placeholder(android.R.drawable.ic_menu_report_image)
            .into(detail_picture)
        detail_text_sport.text = args.storie.sport
        detail_text_title.text = args.storie.title
        detail_text_author.text = "By ${args.storie.author}"
        detail_text_teaser.text = args.storie.teaser

        val timeAgo= DateUtils.getRelativeTimeSpanString(args.storie.date.time,
            Calendar.getInstance().timeInMillis,
            DateUtils.MINUTE_IN_MILLIS)
        detail_text_ago.text = timeAgo

        detail_picture_back.setOnClickListener {
            findNavController().navigateUp()
        }
    }

}