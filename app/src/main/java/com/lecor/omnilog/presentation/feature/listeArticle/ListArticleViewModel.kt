package com.lecor.omnilog.presentation.feature.listeArticle

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lecor.omnilog.data.repository.ListArticleRepository
import com.lecor.omnilog.presentation.`interface`.Article
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ListArticleViewModel(private val listArticleRepository: ListArticleRepository) : ViewModel() {

    init {
        callListArticle()
    }

    private val listArticle = MutableLiveData<MutableList<Article>>()
    fun getListArticle(): LiveData<MutableList<Article>>{
        return listArticle
    }


    private fun callListArticle(){
        viewModelScope.launch(Dispatchers.IO) {
            listArticle.postValue(listArticleRepository.getListArticle().toMutableList())
        }
    }

}
