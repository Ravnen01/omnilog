package com.lecor.omnilog.presentation.`interface`

interface Article {
    fun getTitleArticle():String
    fun getImageArticle():String
    fun getSportArticle():String
    fun getLastLine():String
}