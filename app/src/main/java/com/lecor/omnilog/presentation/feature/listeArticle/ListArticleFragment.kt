package com.lecor.omnilog.presentation.feature.listeArticle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.lecor.omnilog.R
import com.lecor.omnilog.presentation.`interface`.Article
import com.lecor.omnilog.presentation.model.Stories
import kotlinx.android.synthetic.main.list_article_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListArticleFragment : Fragment() , ListArticleRecyclerAdapter.OnItemClickListener {

    private val listArticleViewModel: ListArticleViewModel by viewModel()
    private var listArticleRecyclerAdapter: ListArticleRecyclerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_article_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        initObserver()
    }

    private fun initRecycler() {
        val layoutManager = LinearLayoutManager(requireContext())
        listArticleRecyclerAdapter = ListArticleRecyclerAdapter(requireContext(),this)
        list_article_recycler.layoutManager = layoutManager
        list_article_recycler.adapter = listArticleRecyclerAdapter
    }

    private fun initObserver() {
        listArticleViewModel.getListArticle().observe(viewLifecycleOwner) { listArticle ->
            listArticleRecyclerAdapter?.setListArticle(listArticle)
            listArticleRecyclerAdapter?.notifyDataSetChanged()
        }
    }

    override fun onItemClicked(article: Article) {
        if(article is Stories){
           val action = ListArticleFragmentDirections.actionListArticleFragmentToDetailStorieFragment(article)
           findNavController().navigate(action)
        }
    }


}
